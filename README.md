Hi candidate, this an exercise related to some of the skills that we will need from you if you want to code inside Tulotero SysAdmin team. 

This exercise allows us to evaluate your skills about terraform and some of the most common aws services, but it will cover some knowledge about aws-cli, docker, terragrunt and DB administration.

## Prerequisites in computer

In order to deploy your environment, you will need to be installed a docker service to deploy an image of localstack (a way to mock some of the aws services), docker-compose, terraform, aws-cli, terragrunt and git (to clone our base to start)

First of all, you have to clone this base project and deploy the image with the docker-compose. This will deploy a mock service that we will use to execute the terraform code.

Once you have the image deployed, we will execute the terragrunt with the prod configuration, this must create a base vpc with an ec2 instance. If you try to execute the apply again, you will see changes, dont worry, it's a bug from localstack, you just have to execute the apply command two more times in arrow and you will see the expected output "no changes". 

From there, you can start doing the exercise.

## Exercise

### First step

To start making contact with the code, we suggest a simple task: Change the availability zones, from string to list with two zones. Execute the apply to see if we have created two new subnets

### Second step

We need to generate a backend instance in the private subnet and enable the ssh connection just from the public subnet. So please, code it!!

IMPORTANT: We saw a bug  when you attach the security group to this instance throught some terraform versions, so if you see that the security group breaks the indempotence, please, fix it attaching the sg throught the aws-cli and don't worry about the tfstate it will be fine.

### Third step

Nice! but we need something more. Last days, we saw an increment of the CPU usage from our backend instances, so we decided to create and integrate an alarm which notifies to our alert system (Pager Duty) in case the usage of the CPU in the backend instances increases up to 80%. So, could you make it?

Note: if when you are creating the metric alarm localstack keep creating this resource during 1 minute or more, don't worry, the code could be right and it's failing to create it for other reasons, so if you are sure about your code submit it. We are only interesting in the terraform code so you don't need to call a real endpoint in PD, just understand the integration and automate it with aws. 

When you finish the exercise, create a `git patch` and send it to us if you want to be validated by us.

## Some interesting questions 

Besides the previous patch, we would like you to answer the two following questions in no more than 500 words each one.

1. If you want to develop new services in aws with terraform, how would you manage the terraform state if you are programming with another teammate at the same time in the same account but in differents services?

2. We are interested in your knowledge and experience scaling databases. Suppose you start a Postgresql database in a startup from scratch and eventually it will serve thousands of transactions per second. What problems will you encounter and what solution would you provide?

