locals {
 common_vars = read_terragrunt_config("common.hcl")

 region  = local.common_vars.locals.region
 profile = local.common_vars.locals.profile
}

generate "provider" {
    path = "provider.tf"
    if_exists = "overwrite_terragrunt"
    contents = <<EOF
terraform {
    required_providers {
        aws ={
          source  = "hashicorp/aws"
          version = "~> 3.74.0"
        }
    }
}
provider "aws" {
  profile = "${local.profile}"
  region  = "${local.region}"

  # localstackconfig
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true
  endpoints {
      s3     = "http://localhost:4572"
      ec2    = "http://localhost:4597"
    }

}
EOF
}

remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite"
  }
  config  = {
    bucket  = "terraform-state-tl-tests"
    key     = "terraform-state-candidate/terraform.tfstate"
    region  = local.region
    profile = local.profile
    encrypt = true

    #localstackconfig
    force_path_style            = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    endpoint = "http://localhost:4572"
  }
}

generate "common_variables" {
  path      = "common_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF

variable "env" {
  type    = string
  default = null
}

variable "default_tags" {
  type    = map
  default = null
}
EOF
}
