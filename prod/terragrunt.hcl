locals {
  common_vars = read_terragrunt_config("common.hcl")

  country     = local.common_vars.locals.country
  region      = local.common_vars.locals.region
  env         = "prod"
}

terraform {
    source = "${path_relative_from_include()}//modules"
}

include {
    path = find_in_parent_folders()
}

inputs = {
    #VPC
    availability_zones     = "${local.region}a"
    env                    = local.env
    vpc_cidr               = "172.10.0.0/16"
    vpc_public_cidr        = "172.10.20.0/24"
    vpc_private_cidr       = "172.10.10.0/24"

    #Ssh key
    ec2_ssh_key_pair_name = "ssh_tl"

    #FrontEnd
    ec2_frontend_nodes         = 1
    ec2_frontend_name          = "nginx"
    ec2_frontend_ami           = "ami-0dad359ff462124ca" # Ubuntu 20.04
    ec2_frontend_instance_type = "t2.micro"

    default_tags = {
        Project     = "pruebas_sistemas"
        Environment = local.env
        Terraform   = "true"
    }
}
